package com.enver.nebochat.app;

import android.app.Application;
import android.os.Handler;
import android.support.text.emoji.EmojiCompat;
import android.support.text.emoji.FontRequestEmojiCompatConfig;
import android.support.v4.provider.FontRequest;
import android.util.Log;

import com.enver.nebochat.R;
import com.enver.nebochat.models.IntPreference;
import com.enver.nebochat.models.StringPreference;
import com.enver.nebochat.models.User;
import com.enver.nebochat.synchronization.API;
import com.enver.nebochat.synchronization.Synchronization;
import com.enver.nebochat.utils.Utils;
import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Application Controller class
 *
 * @author Enver
 * @date 05.05.2018.
 */
public class AppController extends Application {

    //final static String BASE_URL = "https://nebochat.000webhostapp.com/nebochat/v1/";
    final static String BASE_URL = "http://192.168.0.105/nebochat/v1/";
    //final static String BASE_URL = "http://10.0.2.2/nebochat/v1/";

    final static int updatePeriod = 1000; //in milliseconds

    final static boolean DEBUG_MODE = true;

    final static int dbSchemaVersion = 1;

    public API api;
    public Gson gson;

    public int userId = -1;
    public String apiKey;

    private byte[] realmEncryptionKey = new byte[]{51, 6, 18, 33, 124, 15, 97, 87, 75, 5, 119, 21, 107, 53, 5, 109, 35, 33, 57, 119, 2, 55, 76, 115, 74, 69, 19, 38, 58, 88, 70, 79, 80, 20, 82, 80, 66, 84, 82, 18, 16, 13, 27, 94, 60, 31, 33, 47, 33, 100, 19, 9, 36, 51, 94, 92, 76, 45, 14, 95, 112, 122, 46, 103};

    private static AppController mInstance;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        gson = new GsonBuilder()
            .registerTypeAdapter(Boolean.class, new Utils.MyBooleanTypeAdapter())
            .registerTypeAdapter(boolean.class, new Utils.MyBooleanTypeAdapter())
            .registerTypeAdapter(Date.class, new Utils.MyDateTypeAdapter())
            .registerTypeAdapter(new TypeToken<RealmList<String>>() {
            }.getType(), new Utils.MyStringArrayTypeAdapter())
            .registerTypeAdapter(new TypeToken<RealmList<User>>() {
            }.getType(), new Utils.MyUserArrayTypeAdapter())
            .setLenient()
            .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(interceptor).build();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Synchronization.performChatSync();

                handler.postDelayed(this, updatePeriod);
            }
        }, updatePeriod);

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

        api = retrofit.create(API.class);

        Realm.init(getApplicationContext());

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
            //.encryptionKey(realmEncryptionKey)
            .schemaVersion(dbSchemaVersion)
            .build();

        Realm.setDefaultConfiguration(realmConfiguration);

        try (Realm realm = Realm.getDefaultInstance()) {
            IntPreference userIdPreference = realm.where(IntPreference.class).equalTo("name", "userId").findFirst();

            if (userIdPreference == null)
                throw new NullPointerException();

            User user = realm.where(User.class).equalTo("id", userIdPreference.value).findFirst();

            if (user == null)
                throw new NullPointerException();

            userId = user.id;

            StringPreference apiKeyPreference = realm.where(StringPreference.class).equalTo("name", "apiKey").findFirst();

            if (apiKeyPreference == null)
                throw new NullPointerException();

            apiKey = apiKeyPreference.value;
        } catch (NullPointerException e) {
            userId = -1;
            apiKey = null;
            Log.d("NeboChat", "Not logged in!");
        }

        FontRequest fontRequest = new FontRequest("com.example.fontprovider", "com.example", "emoji compat Font Query", R.array.com_google_android_gms_fonts_certs);
        EmojiCompat.Config config = new FontRequestEmojiCompatConfig(this, fontRequest);
        EmojiCompat.init(config);

        Synchronization.performFullSync();

        if (DEBUG_MODE)
            Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                    .build());
    }
}
