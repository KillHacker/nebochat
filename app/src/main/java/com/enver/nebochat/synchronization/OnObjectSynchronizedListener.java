package com.enver.nebochat.synchronization;

/**
 * Application Controller class
 *
 * @author Enver
 * @date 13.05.2018.
 */
public interface OnObjectSynchronizedListener<T> {

    void OnObjectSynchronized(T object);
}
