package com.enver.nebochat.synchronization;

import com.enver.nebochat.models.Chat;
import com.enver.nebochat.models.Friendship;
import com.enver.nebochat.models.Message;
import com.enver.nebochat.models.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * API class
 *
 * @author Enver
 * @date 05.05.2018.
 */
public interface API {

    class Response {
        public boolean error;
        public String message;
        public int code;
    }

    class RegisterResponse extends Response {
        public User user;
        public String apiKey;
    }

    @FormUrlEncoded
    @POST("register")
    Call<RegisterResponse> addUser(@Field("nickname") String nickname, @Field("password") String password, @Field("place") String place, @Field("email") String email, @Field("birthday") String birthday, @Field("gender") int gender);

    class LoginResponse extends Response {
        public User user;
        public String apiKey;
    }

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(@Field("emailNickname") String emailNickname, @Field("usingEmail") boolean usingEmail, @Field("password") String password);

    class GetObjectResponse<T> extends Response {
        public T object;
    }

    @FormUrlEncoded
    @POST("getObject")
    Call<GetObjectResponse<User>> getUser(@Field("authorization") String apiKey, @Field("type") String type, @Field("id") int id);

    @FormUrlEncoded
    @POST("getObject")
    Call<GetObjectResponse<Message>> getMessage(@Field("authorization") String apiKey, @Field("type") String type, @Field("id") int id);

    @FormUrlEncoded
    @POST("getObject")
    Call<GetObjectResponse<Friendship>> getFriendship(@Field("authorization") String apiKey, @Field("type") String type, @Field("id") int id);

    @FormUrlEncoded
    @POST("getObject")
    Call<GetObjectResponse<Chat>> getChat(@Field("authorization") String apiKey, @Field("type") String type, @Field("id") int id);

    class GetVersionResponse extends Response {
        public long version;
    }

    @FormUrlEncoded
    @POST("getVersion")
    Call<GetVersionResponse> getVersion(@Field("authorization") String apiKey, @Field("type") String type, @Field("id") int id);

    class GetMyChatsResponse extends Response {
        int[] chatIds;
        int[] messageIds;
    }

    @FormUrlEncoded
    @POST("getMyChats")
    Call<GetMyChatsResponse> getMyChats(@Field("authorization") String apiKey);

    @FormUrlEncoded
    @POST("sendMessage")
    Call<GetObjectResponse<Message>> sendMessage(@Field("authorization") String apiKey, @Field("chatId") int chatId, @Field("message") String message);
}
