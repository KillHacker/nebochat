package com.enver.nebochat.synchronization;

import android.util.Log;

import com.enver.nebochat.app.AppController;
import com.enver.nebochat.models.Chat;
import com.enver.nebochat.models.Friendship;
import com.enver.nebochat.models.Message;
import com.enver.nebochat.models.User;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Synchronization class
 *
 * @author Enver
 * @date 12.05.2018.
 */
public class Synchronization {

    private static Realm realm = Realm.getDefaultInstance();

    public static void performFullSync() {
        final String apiKey = AppController.getInstance().apiKey;
        final API api = AppController.getInstance().api;

        if (realm == null)
            realm = Realm.getDefaultInstance();

        syncClassUser(realm, api, apiKey, User.class);
        syncClassMessage(realm, api, apiKey, Message.class);
        syncClassFriendship(realm, api, apiKey, Friendship.class);
        syncClassChat(realm, api, apiKey, Chat.class);

        syncMyChats(realm, api, apiKey);
    }

    public static void performChatSync() {
        final String apiKey = AppController.getInstance().apiKey;
        final API api = AppController.getInstance().api;

        if (realm == null)
            realm = Realm.getDefaultInstance();

        syncMyChats(realm, api, apiKey);
    }

    private static void syncClassUser(Realm realm, API api, String apiKey, Class<User> classToSync) {
        for (final User t : realm.where(classToSync).findAll()) {
            if (t.modified) {

            } else
                api.getVersion(apiKey, "user", t.id).enqueue(new Callback<API.GetVersionResponse>() {
                    @Override
                    public void onResponse(Call<API.GetVersionResponse> call, Response<API.GetVersionResponse> response) {
                        API.GetVersionResponse getVersionResponse = response.body();

                        if (getVersionResponse != null)
                            switch (getVersionResponse.code) {
                                case 600:
                                    if (getVersionResponse.version > t.version)
                                        t.synchronizeObject();

                                    break;
                                case 601:
                                    Log.e("Synchronization", getVersionResponse.message);
                                    break;
                            }
                    }

                    @Override
                    public void onFailure(Call<API.GetVersionResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        }
    }

    private static void syncClassMessage(Realm realm, API api, String apiKey, Class<Message> classToSync) {
        for (final Message t : realm.where(classToSync).findAll()) {
            if (t.modified) {

            } else
                api.getVersion(apiKey, "message", t.id).enqueue(new Callback<API.GetVersionResponse>() {
                    @Override
                    public void onResponse(Call<API.GetVersionResponse> call, Response<API.GetVersionResponse> response) {
                        API.GetVersionResponse getVersionResponse = response.body();

                        if (getVersionResponse != null)
                            switch (getVersionResponse.code) {
                                case 600:
                                    if (getVersionResponse.version > t.version)
                                        t.synchronizeObject();

                                    break;
                                case 601:
                                    Log.e("Synchronization", getVersionResponse.message);
                                    break;
                            }
                    }

                    @Override
                    public void onFailure(Call<API.GetVersionResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        }
    }

    private static void syncClassFriendship(Realm realm, API api, String apiKey, Class<Friendship> classToSync) {
        for (final Friendship t : realm.where(classToSync).findAll()) {
            if (t.modified) {

            } else
                api.getVersion(apiKey, "friendship", t.id).enqueue(new Callback<API.GetVersionResponse>() {
                    @Override
                    public void onResponse(Call<API.GetVersionResponse> call, Response<API.GetVersionResponse> response) {
                        API.GetVersionResponse getVersionResponse = response.body();

                        if (getVersionResponse != null)
                            switch (getVersionResponse.code) {
                                case 600:
                                    if (getVersionResponse.version > t.version)
                                        t.synchronizeObject();

                                    break;
                                case 601:
                                    Log.e("Synchronization", getVersionResponse.message);
                                    break;
                            }
                    }

                    @Override
                    public void onFailure(Call<API.GetVersionResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        }
    }

    private static void syncClassChat(Realm realm, API api, String apiKey, Class<Chat> classToSync) {
        for (final Chat t : realm.where(classToSync).findAll()) {
            if (t.modified) {

            } else
                api.getVersion(apiKey, "chat", t.id).enqueue(new Callback<API.GetVersionResponse>() {
                    @Override
                    public void onResponse(Call<API.GetVersionResponse> call, Response<API.GetVersionResponse> response) {
                        API.GetVersionResponse getVersionResponse = response.body();

                        if (getVersionResponse != null)
                            switch (getVersionResponse.code) {
                                case 600:
                                    if (getVersionResponse.version > t.version)
                                        t.synchronizeObject();

                                    break;
                                case 601:
                                    Log.e("Synchronization", getVersionResponse.message);
                                    break;
                            }
                    }

                    @Override
                    public void onFailure(Call<API.GetVersionResponse> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        }
    }

    private static void syncMyChats(final Realm realm, final API api, final String apiKey) {
        api.getMyChats(apiKey).enqueue(new Callback<API.GetMyChatsResponse>() {
            @Override
            public void onResponse(Call<API.GetMyChatsResponse> call, Response<API.GetMyChatsResponse> response) {
                final API.GetMyChatsResponse getMyChatsResponse = response.body();

                if (getMyChatsResponse != null)
                    switch (getMyChatsResponse.code) {
                        case 604:
                            for (int chatId : getMyChatsResponse.chatIds)
                                if (realm.where(Chat.class).equalTo("id", chatId).findAll().size() == 0)
                                    new Chat(chatId).synchronizeObject();

                            for (int messageId : getMyChatsResponse.messageIds)
                                if (realm.where(Message.class).equalTo("id", messageId).findAll().size() == 0)
                                    new Message(messageId).synchronizeObject();

                            break;
                        case 605:

                            break;
                    }
            }

            @Override
            public void onFailure(Call<API.GetMyChatsResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
