package com.enver.nebochat.models;

import com.enver.nebochat.app.AppController;
import com.enver.nebochat.synchronization.API;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Friendship model class
 *
 * @author Enver
 * @date 02.05.2018.
 */
public class Friendship extends RealmObject {

    @PrimaryKey
    public int id;

    //False when synchronized, becomes true whenever object changes locally
    public boolean modified = false;

    public long version = -1;

    public User firstUser;

    public User secondUser;

    public boolean accepted = false;

    public void synchronizeObject() {
        AppController.getInstance().api.getFriendship(AppController.getInstance().apiKey, this.getClass().getSimpleName(), id).enqueue(new Callback<API.GetObjectResponse<Friendship>>() {
            @Override
            public void onResponse(Call<API.GetObjectResponse<Friendship>> call, Response<API.GetObjectResponse<Friendship>> response) {
                final API.GetObjectResponse<Friendship> getObjectResponse = response.body();

                if (getObjectResponse != null)
                    switch (getObjectResponse.code) {
                        case 600:
                            Realm realm = Realm.getDefaultInstance();

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.insertOrUpdate(getObjectResponse.object);
                                }
                            });

                            realm.close();
                            break;
                        case 601:

                            break;
                    }
            }

            @Override
            public void onFailure(Call<API.GetObjectResponse<Friendship>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
