package com.enver.nebochat.models;

import android.util.Log;

import com.enver.nebochat.app.AppController;
import com.enver.nebochat.synchronization.API;
import com.enver.nebochat.synchronization.OnObjectSynchronizedListener;

import java.sql.Timestamp;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Message model class
 *
 * @author Enver
 * @date 01.05.2018.
 */
public class Message extends RealmObject {

    @PrimaryKey
    public int id;

    //False when synchronized, becomes true whenever object changes locally
    public boolean modified = false;

    public long version = -1;

    public User user;

    public int chatId;

    @Required
    public String message;

    @Required
    public Date sent;

    public boolean deliveredToServer = false;

    public boolean delivered = false;

    public RealmList<User> userSeen = new RealmList<>();

    public Message() {
    }

    public Message(int chatId, String message) {
        this.chatId = chatId;
        this.message = message;
        user = new User(AppController.getInstance().userId);
        sent = new Date();
    }

    public Message(int id) {
        this.id = id;
    }

    public String getSentText() {
        Timestamp now = new Timestamp(System.currentTimeMillis());

        if (now.getDate() == sent.getDate())
            return sent.getHours() + ":" + sent.getMinutes();
        else
            return sent.getDay() + "." + sent.getMonth() + ".";
    }

    public void send() {
        AppController.getInstance().api.sendMessage(AppController.getInstance().apiKey, chatId, message).enqueue(new Callback<API.GetObjectResponse<Message>>() {
            @Override
            public void onResponse(Call<API.GetObjectResponse<Message>> call, Response<API.GetObjectResponse<Message>> response) {
                final API.GetObjectResponse<Message> getObjectResponse = response.body();

                if (getObjectResponse != null) {
                    switch (getObjectResponse.code) {
                        case 600:
                            Realm realm = Realm.getDefaultInstance();

                            int userId = getObjectResponse.object.user.id;
                            getObjectResponse.object.user = realm.where(User.class).equalTo("id", userId).findFirst();

                            if (getObjectResponse.object.user == null)
                                new User(userId).synchronizeObject(new OnObjectSynchronizedListener<User>() {
                                    @Override
                                    public void OnObjectSynchronized(User object) {
                                        Realm realm = Realm.getDefaultInstance();
                                        getObjectResponse.object.user = object;
                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.insertOrUpdate(getObjectResponse.object);
                                            }
                                        });

                                        realm.close();
                                    }
                                });
                            else
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.insertOrUpdate(getObjectResponse.object);
                                    }
                                });

                            realm.close();
                            Log.d("Message", "Sent: " + message);
                            break;
                        case 601:
                            //Inserted into db but couldn't retrieve it
                            Log.e("Message", getObjectResponse.message);
                            break;
                        case 605:
                            //Not inserted in the db
                            Log.e("Message", getObjectResponse.message);
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<API.GetObjectResponse<Message>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void synchronizeObject() {
        AppController.getInstance().api.getMessage(AppController.getInstance().apiKey, this.getClass().getSimpleName(), id).enqueue(new Callback<API.GetObjectResponse<Message>>() {
            @Override
            public void onResponse(Call<API.GetObjectResponse<Message>> call, Response<API.GetObjectResponse<Message>> response) {
                final API.GetObjectResponse<Message> getObjectResponse = response.body();

                if (getObjectResponse != null)
                    switch (getObjectResponse.code) {
                        case 600:
                            Realm realm = Realm.getDefaultInstance();

                            int userId = getObjectResponse.object.user.id;
                            getObjectResponse.object.user = realm.where(User.class).equalTo("id", userId).findFirst();

                            if (getObjectResponse.object.user == null)
                                new User(userId).synchronizeObject(new OnObjectSynchronizedListener<User>() {
                                    @Override
                                    public void OnObjectSynchronized(User object) {
                                        Realm realm = Realm.getDefaultInstance();
                                        getObjectResponse.object.user = object;
                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.insertOrUpdate(getObjectResponse.object);
                                            }
                                        });

                                        realm.close();
                                    }
                                });
                            else
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.insertOrUpdate(getObjectResponse.object);
                                    }
                                });

                            realm.close();
                            break;
                        case 601:

                            break;
                    }
            }

            @Override
            public void onFailure(Call<API.GetObjectResponse<Message>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
