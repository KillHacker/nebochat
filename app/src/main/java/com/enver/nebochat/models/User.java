package com.enver.nebochat.models;

import com.enver.nebochat.app.AppController;
import com.enver.nebochat.synchronization.API;
import com.enver.nebochat.synchronization.OnObjectSynchronizedListener;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * User model class
 *
 * @author Enver
 * @date 01.05.2018.
 */
public class User extends RealmObject {

    @PrimaryKey
    public int id;

    //False when synchronized, becomes true whenever object changes locally
    public boolean modified = false;

    public long version = 1;

    @Required
    public String nickname;

    public String email;

    public RealmList<String> locations;

    public boolean verified = false;

    public Date dateOfBirth;

    public int gender;

    public Date created;

    public int participantsCount = 5;

    public int maxParallelParticipations = 5;

    public int ratingAverage;

    public int ratingCount = 0;

    public RealmList<User> blockedUsers = new RealmList<>();

    public User() {
    }

    public User(int id) {
        this.id = id;
    }

    public final static class Gender {
        public final static int NOT_KNOWN = 0;
        public final static int MALE = 1;
        public final static int FEMALE = 2;
        public final static int NOT_APPLICABLE = 9;
    }

    public void synchronizeObject() {
        synchronizeObject(null);
    }

    public void synchronizeObject(final OnObjectSynchronizedListener<User> userOnObjectSynchronizedListener) {
        AppController.getInstance().api.getUser(AppController.getInstance().apiKey, this.getClass().getSimpleName(), id).enqueue(new Callback<API.GetObjectResponse<User>>() {
            @Override
            public void onResponse(Call<API.GetObjectResponse<User>> call, Response<API.GetObjectResponse<User>> response) {
                final API.GetObjectResponse<User> getObjectResponse = response.body();

                if (getObjectResponse != null)
                    switch (getObjectResponse.code) {
                        case 600:
                            Realm realm = Realm.getDefaultInstance();

                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.insertOrUpdate(getObjectResponse.object);
                                }
                            });

                            realm.close();

                            if (userOnObjectSynchronizedListener != null)
                                userOnObjectSynchronizedListener.OnObjectSynchronized(getObjectResponse.object);
                            break;
                        case 601:

                            break;
                    }
            }

            @Override
            public void onFailure(Call<API.GetObjectResponse<User>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
