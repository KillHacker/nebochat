package com.enver.nebochat.models;

import com.enver.nebochat.app.AppController;
import com.enver.nebochat.synchronization.API;
import com.enver.nebochat.synchronization.OnObjectSynchronizedListener;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Chat model class
 *
 * @author Enver
 * @date 01.05.2018.
 */
public class Chat extends RealmObject {

    @PrimaryKey
    public int id;

    //False when synchronized, becomes true whenever object changes locally
    public boolean modified = false;

    public long version = -1;

    @Required
    public String name;

    public User keyUser;

    public RealmList<User> participants;

    @Required
    public Date created;

    public Chat() {
    }

    public Chat(int id) {
        this.id = id;
    }

    public void synchronizeObject() {
        synchronizeObject(null);
    }

    public void synchronizeObject(final OnObjectSynchronizedListener<Chat> onObjectSynchronizedListener) {
        AppController.getInstance().api.getChat(AppController.getInstance().apiKey, "chat", id).enqueue(new Callback<API.GetObjectResponse<Chat>>() {
            @Override
            public void onResponse(Call<API.GetObjectResponse<Chat>> call, Response<API.GetObjectResponse<Chat>> response) {
                final API.GetObjectResponse<Chat> getObjectResponse = response.body();

                if (getObjectResponse != null)
                    switch (getObjectResponse.code) {
                        case 600:
                            Realm realm = Realm.getDefaultInstance();

                            int keyUserId = getObjectResponse.object.keyUser.id;
                            getObjectResponse.object.keyUser = realm.where(User.class).equalTo("id", keyUserId).findFirst();

                            if (getObjectResponse.object.keyUser == null)
                                new User(keyUserId).synchronizeObject(new OnObjectSynchronizedListener<User>() {
                                    @Override
                                    public void OnObjectSynchronized(User object) {
                                        Realm realm = Realm.getDefaultInstance();
                                        getObjectResponse.object.keyUser = object;
                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.insertOrUpdate(getObjectResponse.object);
                                            }
                                        });

                                        if (onObjectSynchronizedListener != null)
                                            onObjectSynchronizedListener.OnObjectSynchronized(getObjectResponse.object);

                                        realm.close();
                                    }
                                });
                            else {
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        realm.insertOrUpdate(getObjectResponse.object);
                                    }
                                });

                                if (onObjectSynchronizedListener != null)
                                    onObjectSynchronizedListener.OnObjectSynchronized(getObjectResponse.object);
                            }

                            realm.close();
                            break;
                        case 601:

                            break;
                    }
            }

            @Override
            public void onFailure(Call<API.GetObjectResponse<Chat>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
