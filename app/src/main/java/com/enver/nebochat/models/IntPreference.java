package com.enver.nebochat.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Int Preference Model class
 *
 * @author Enver
 * @date 11.05.2018.
 */
public class IntPreference extends RealmObject {

    @PrimaryKey
    public String name;

    public int value;

    public IntPreference() {

    }

    public IntPreference(String name, int value) {
        this.name = name;
        this.value = value;
    }
}