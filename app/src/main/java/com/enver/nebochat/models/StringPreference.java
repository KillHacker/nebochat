package com.enver.nebochat.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * String Preference Model class
 *
 * @author Enver
 * @date 11.05.2018.
 */
public class StringPreference extends RealmObject {

    @PrimaryKey
    public String name;

    public String value;

    public StringPreference() {

    }

    public StringPreference(String name, String value) {
        this.name = name;
        this.value = value;
    }
}