package com.enver.nebochat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.text.emoji.widget.EmojiEditText;
import android.support.text.emoji.widget.EmojiTextView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.enver.nebochat.R;
import com.enver.nebochat.app.AppController;
import com.enver.nebochat.models.Chat;
import com.enver.nebochat.models.Message;
import com.enver.nebochat.synchronization.OnObjectSynchronizedListener;
import com.enver.nebochat.synchronization.Synchronization;
import com.enver.nebochat.utils.MessageDiffCallback;
import com.enver.nebochat.utils.Utils;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class ChatActivity extends AppCompatActivity implements Utils.KeyboardVisibilityListener, RealmChangeListener<RealmResults<Message>> {

    RecyclerView recyclerView;
    MessagesAdapter messagesAdapter;
    EmojiEditText messageView;

    int chatId;
    Realm realm;

    List<Message> messages = new ArrayList<>();
    RealmResults<Message> realmResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Intent intent = getIntent();
        chatId = intent.getIntExtra("chatId", -1);

        if (chatId == -1) {
            Toast.makeText(this, "Chat not selected!", Toast.LENGTH_LONG).show();

            return;
        }

        realm = Realm.getDefaultInstance();

        Chat chat = realm.where(Chat.class).equalTo("id", chatId).findFirst();

        if (chat == null) {
            chat = new Chat(chatId);
            chat.synchronizeObject(new OnObjectSynchronizedListener<Chat>() {
                @Override
                public void OnObjectSynchronized(Chat object) {
                    Intent restartIntent = new Intent(ChatActivity.this, ChatActivity.class);
                    restartIntent.putExtra("roomId", chatId);
                    startActivity(restartIntent);
                }
            });

            return;
        }

        EmojiTextView chatNameView = findViewById(R.id.chat_name);
        chatNameView.setText(StringEscapeUtils.unescapeJava(chat.name));
        messageView = findViewById(R.id.message_view);

        recyclerView = findViewById(R.id.messages_view);
        messagesAdapter = new MessagesAdapter();
        recyclerView.setAdapter(messagesAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(this) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                try {
                    super.onLayoutChildren(recycler, state);
                } catch (IndexOutOfBoundsException e) {
                    Log.e("probe", "meet a IOOBE in RecyclerView");
                }
            }
        });

        Utils.setKeyboardVisibilityListener(this, this);

        realmResults = realm.where(Message.class).equalTo("chatId", chatId).findAll();
        realmResults.addChangeListener(this);

        onChange(realmResults);

        if (messages.size() > 0)
            recyclerView.scrollToPosition(messages.size() - 1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        realm.close();
    }

    @Override
    public void onChange(@NonNull RealmResults<Message> messagesResults) {
        boolean scrolledToBottom = !recyclerView.canScrollVertically(1);

        List<Message> refreshedMessages = new ArrayList<>(messagesResults);

        final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new MessageDiffCallback(messages, refreshedMessages), true);
        messages = refreshedMessages;

        result.dispatchUpdatesTo(messagesAdapter);

        if (scrolledToBottom && messages.size() > 0)
            recyclerView.scrollToPosition(messages.size() - 1);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendButton:
                String message = messageView.getText().toString();
                message = StringEscapeUtils.escapeJava(message);

                new Message(chatId, message).send();
                messageView.setText("");
                break;
            case R.id.back_button:
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Synchronization.performFullSync();
    }

    @Override
    public void onKeyboardVisibilityChanged(boolean keyboardVisible) {
        if (keyboardVisible && messages.size() > 0)
            recyclerView.scrollToPosition(messages.size() - 1);
    }

    public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolderMessage> {

        ViewHolderMessage focusedMessage;

        @Override
        public ViewHolderMessage onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            switch (viewType) {
                case 0:
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_other, parent, false);
                    return new ViewHolderOther(v);
                case 1:
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_me, parent, false);
                    return new ViewHolderMe(v);
                case 2:
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_other_extended, parent, false);
                    return new ViewHolderMessage(v);
                case 3:
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_me_extended, parent, false);
                    return new ViewHolderMessage(v);
                default:
                    return null;
            }
        }

        @Override
        public void onBindViewHolder(ViewHolderMessage holder, int position) {

            Message message = messages.get(position);
            holder.messageView.setText(StringEscapeUtils.unescapeJava(message.message));
            holder.sentView.setText(message.getSentText());

            switch (holder.getItemViewType()) {
                case 0:
                    ViewHolderOther viewHolderOther = (ViewHolderOther) holder;

                    viewHolderOther.userNameView.setText(StringEscapeUtils.unescapeJava(message.user.nickname));
                    //viewHolderOther.profileIconView.setImageDrawable();
                    break;
                case 1:
                    ViewHolderMe viewHolderMe = (ViewHolderMe) holder;

                    //viewHolderMe.profileIconView.setImageDrawable();
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0 || messages.get(position - 1).user.id != messages.get(position).user.id)
                return messages.get(position).user.id == AppController.getInstance().userId ? 1 : 0;
            else
                return messages.get(position).user.id == AppController.getInstance().userId ? 3 : 2;
        }

        class ViewHolderMessage extends RecyclerView.ViewHolder implements View.OnClickListener {

            EmojiTextView messageView;
            TextView sentView;

            ViewHolderMessage(View itemView) {
                super(itemView);

                messageView = itemView.findViewById(R.id.message_view);
                sentView = itemView.findViewById(R.id.sent_view);

                itemView.setOnClickListener(this);
            }

            void setFocused(boolean focused) {
                if (getItemViewType() == 2 || getItemViewType() == 3)
                    sentView.setVisibility(focused ? View.VISIBLE : View.INVISIBLE);

                if (focusedMessage != null && focusedMessage != this)
                    focusedMessage.setFocused(false);

                focusedMessage = focused ? this : null;
            }

            @Override
            public void onClick(View view) {
                setFocused(focusedMessage != this);
            }
        }

        class ViewHolderOther extends ViewHolderMessage {

            TextView userNameView;
            CircleImageView profileIconView;

            ViewHolderOther(View itemView) {
                super(itemView);

                userNameView = itemView.findViewById(R.id.user_name_view);
                profileIconView = itemView.findViewById(R.id.profile_icon_view);
            }
        }

        class ViewHolderMe extends ViewHolderMessage {

            CircleImageView profileIconView;

            ViewHolderMe(View itemView) {
                super(itemView);

                profileIconView = itemView.findViewById(R.id.profile_icon_view);
            }
        }
    }
}
