package com.enver.nebochat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.enver.nebochat.R;
import com.enver.nebochat.app.AppController;
import com.enver.nebochat.models.IntPreference;
import com.enver.nebochat.models.StringPreference;
import com.enver.nebochat.synchronization.API;
import com.enver.nebochat.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout emailNicknameInputLayout, passwordInputLayout;
    TextView emailNicknameTextView, passwordTextView;

    CallbackManager callbackManager;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (AppController.getInstance().userId != -1) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        setupFacebookLogin();

        emailNicknameInputLayout = findViewById(R.id.input_layout_email_nickname);
        emailNicknameTextView = findViewById(R.id.input_email_nickname);
        passwordInputLayout = findViewById(R.id.input_layout_password);
        passwordTextView = findViewById(R.id.input_password);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                boolean error = false;

                boolean usingEmail = false;
                String emailNickname = emailNicknameTextView.getText().toString();
                if (emailNickname.isEmpty()) {
                    error = true;
                    emailNicknameInputLayout.setErrorEnabled(true);
                    emailNicknameInputLayout.setError("Nickname or email is required");
                } else {
                    emailNicknameInputLayout.setErrorEnabled(false);
                    usingEmail = Utils.isValidEmail(emailNickname);
                }

                String password = passwordTextView.getText().toString();
                if (password.isEmpty()) {
                    error = true;
                    passwordInputLayout.setErrorEnabled(true);
                    passwordInputLayout.setError("Password is required");
                } else if (password.length() < 5 || password.length() > 15) {
                    error = true;
                    passwordInputLayout.setErrorEnabled(true);
                    passwordInputLayout.setError("Invalid password");
                } else
                    passwordInputLayout.setErrorEnabled(false);

                if (!error) {
                    AppController.getInstance().api.login(emailNickname, usingEmail, password).enqueue(new Callback<API.LoginResponse>() {
                        @Override
                        public void onResponse(Call<API.LoginResponse> call, Response<API.LoginResponse> response) {
                            final API.LoginResponse loginResponse = response.body();

                            if (loginResponse != null) {
                                switch (loginResponse.code) {
                                    case 600:

                                        AppController appController = AppController.getInstance();
                                        Realm realm = Realm.getDefaultInstance();

                                        appController.userId = loginResponse.user.id;
                                        appController.apiKey = loginResponse.apiKey;

                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.insertOrUpdate(loginResponse.user);
                                                realm.insertOrUpdate(new IntPreference("userId", loginResponse.user.id));
                                                realm.insertOrUpdate(new StringPreference("apiKey", loginResponse.apiKey));
                                            }
                                        });

                                        realm.close();

                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        break;
                                    case 601:
                                        Toast.makeText(LoginActivity.this, loginResponse.message, Toast.LENGTH_LONG).show();
                                        break;
                                    case 602:
                                        passwordInputLayout.setErrorEnabled(true);
                                        passwordInputLayout.setError("Incorrect credentials!");
                                        break;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<API.LoginResponse> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, "An error occurred!", Toast.LENGTH_LONG).show();
                            t.printStackTrace();
                        }
                    });
                }
                break;
            case R.id.forgot_password_view:
                Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_LONG).show();
                break;
            case R.id.facebook_button:
                Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_LONG).show();
                //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday"));
                break;
            case R.id.twitter_button:
                Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_LONG).show();
                break;
            case R.id.instagram_button:
                Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_LONG).show();
                break;
            case R.id.google_plus_button:
                Toast.makeText(this, "Not implemented yet!", Toast.LENGTH_LONG).show();
                break;
            case R.id.register_button:
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setupFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.v("LoginActivity", response.toString());

                            // Application code
                            //String email = object.getString("email");
                            //String birthday = object.getString("birthday"); // 01/31/1980 format
                        }
                    });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });
    }
}
