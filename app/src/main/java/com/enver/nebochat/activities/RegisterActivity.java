package com.enver.nebochat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.enver.nebochat.R;
import com.enver.nebochat.app.AppController;
import com.enver.nebochat.models.IntPreference;
import com.enver.nebochat.models.StringPreference;
import com.enver.nebochat.synchronization.API;
import com.enver.nebochat.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 253;

    Button genderNaButton, genderMaleButton, genderFemaleButton;
    Spinner daySpinner, monthSpinner, yearSpinner;

    TextInputLayout nicknameInputLayout, passwordInputLayout, locationInputLayout, emailInputLayout;
    TextView nicknameTextView, passwordTextView, locationTextView, emailTextView;

    Place selectedPlace;

    int gender = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setupBirthdayViews();

        genderNaButton = findViewById(R.id.gender_na_button);
        genderMaleButton = findViewById(R.id.gender_male_button);
        genderFemaleButton = findViewById(R.id.gender_female_button);

        nicknameTextView = findViewById(R.id.input_nickname);
        passwordTextView = findViewById(R.id.input_password);
        locationTextView = findViewById(R.id.input_location);
        emailTextView = findViewById(R.id.input_email);

        nicknameInputLayout = findViewById(R.id.input_layout_nickname);
        passwordInputLayout = findViewById(R.id.input_layout_password);
        locationInputLayout = findViewById(R.id.input_layout_location);
        emailInputLayout = findViewById(R.id.input_layout_email);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                selectedPlace = PlaceAutocomplete.getPlace(this, data);
                locationTextView.setText(selectedPlace.getAddress());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("RegisterActivity", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gender_na_button:
                gender = 0;
                genderNaButton.setBackgroundResource(R.drawable.toggle_button_on);
                genderMaleButton.setBackgroundResource(R.drawable.toggle_button_off);
                genderFemaleButton.setBackgroundResource(R.drawable.toggle_button_off);
                break;
            case R.id.gender_male_button:
                gender = 1;
                genderNaButton.setBackgroundResource(R.drawable.toggle_button_off);
                genderMaleButton.setBackgroundResource(R.drawable.toggle_button_on);
                genderFemaleButton.setBackgroundResource(R.drawable.toggle_button_off);
                break;
            case R.id.gender_female_button:
                gender = 2;
                genderNaButton.setBackgroundResource(R.drawable.toggle_button_off);
                genderMaleButton.setBackgroundResource(R.drawable.toggle_button_off);
                genderFemaleButton.setBackgroundResource(R.drawable.toggle_button_on);
                break;
            case R.id.input_location:
                try {
                    PlaceAutocomplete.IntentBuilder intentBuilder = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY);
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
                    intentBuilder.setFilter(typeFilter);
                    Intent intent = intentBuilder.build(RegisterActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    // TODO: Handle the error.
                } catch (GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                break;
            case R.id.register_button:
                boolean error = false;

                String nickname = nicknameTextView.getText().toString();
                if (nickname.isEmpty()) {
                    error = true;
                    nicknameInputLayout.setErrorEnabled(true);
                    nicknameInputLayout.setError("Nickname is required");
                } else if (nickname.length() < 5 || nickname.length() > 15) {
                    error = true;
                    nicknameInputLayout.setErrorEnabled(true);
                    nicknameInputLayout.setError("Must be between 5 and 15 chars long");
                } else
                    nicknameInputLayout.setErrorEnabled(false);

                String password = passwordTextView.getText().toString();
                if (password.isEmpty()) {
                    error = true;
                    passwordInputLayout.setErrorEnabled(true);
                    passwordInputLayout.setError("Password is required");
                } else if (password.length() < 5 || password.length() > 15) {
                    error = true;
                    passwordInputLayout.setErrorEnabled(true);
                    passwordInputLayout.setError("Must be between 5 and 15 chars long");
                } else
                    passwordInputLayout.setErrorEnabled(false);

                String place = "";
                if (selectedPlace == null) {
                    error = true;
                    locationInputLayout.setErrorEnabled(true);
                    locationInputLayout.setError("Location is required");
                } else {
                    locationInputLayout.setErrorEnabled(false);
                    place = selectedPlace.getId();
                }

                String email = emailTextView.getText().toString();
                if (!email.isEmpty() && !Utils.isValidEmail(email)) {
                    error = true;
                    emailInputLayout.setErrorEnabled(true);
                    emailInputLayout.setError("Invalid email");
                } else
                    emailInputLayout.setErrorEnabled(false);
                if (email.isEmpty())
                    email = "null";

                String dateOfBirth = "";
                int day = daySpinner.getSelectedItemPosition(),
                    month = monthSpinner.getSelectedItemPosition(),
                    year = yearSpinner.getSelectedItemPosition();
                if (day != 0 && month != 0 && year != 0 && Utils.validDay(day + 2, month + 1, 2010 - year + 1))
                    dateOfBirth = (2010 - year + 1) + "-" + month + "-" + day;
                if (dateOfBirth.isEmpty())
                    dateOfBirth = "null";

                if (!error) {
                    AppController.getInstance().api.addUser(nickname, password, place, email, dateOfBirth, gender).enqueue(new Callback<API.RegisterResponse>() {
                        @Override
                        public void onResponse(Call<API.RegisterResponse> call, Response<API.RegisterResponse> response) {
                            final API.RegisterResponse registerResponse = response.body();

                            if (registerResponse != null) {
                                switch (registerResponse.code) {
                                    case 600:
                                        AppController appController = AppController.getInstance();
                                        Realm realm = Realm.getDefaultInstance();

                                        appController.userId = registerResponse.user.id;
                                        appController.apiKey = registerResponse.apiKey;

                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                realm.insertOrUpdate(registerResponse.user);
                                                realm.insertOrUpdate(new IntPreference("userId", registerResponse.user.id));
                                                realm.insertOrUpdate(new StringPreference("apiKey", registerResponse.apiKey));
                                            }
                                        });

                                        realm.close();

                                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);

                                        break;
                                    case 601:
                                        Toast.makeText(RegisterActivity.this, registerResponse.message, Toast.LENGTH_LONG).show();
                                        break;
                                    case 602:
                                        nicknameInputLayout.setErrorEnabled(true);
                                        nicknameInputLayout.setError("Nickname is already taken!");
                                        break;
                                    case 603:
                                        emailInputLayout.setErrorEnabled(true);
                                        emailInputLayout.setError("Email is already taken!");
                                        break;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<API.RegisterResponse> call, Throwable t) {
                            Toast.makeText(RegisterActivity.this, "An error occurred!", Toast.LENGTH_LONG).show();
                            t.printStackTrace();
                        }
                    });
                }

                break;
        }
    }

    void setupBirthdayViews() {
        daySpinner = findViewById(R.id.day_spinner);
        monthSpinner = findViewById(R.id.month_spinner);
        yearSpinner = findViewById(R.id.year_spinner);

        List<String> itemsDay = new ArrayList<>();
        itemsDay.add("Day");
        for (int i = 1; i <= 31; i++)
            itemsDay.add(i + "");
        ArrayAdapter<String> adapterDay = new ArrayAdapter<>(this, R.layout.item_simple_spinner, itemsDay);
        adapterDay.setDropDownViewResource(R.layout.item_simple_spinner_dropdown);
        daySpinner.setAdapter(adapterDay);

        List<String> itemsMonth = new ArrayList<>();
        itemsMonth.add("Month");
        for (int i = 1; i <= 12; i++)
            itemsMonth.add(i + "");
        ArrayAdapter<String> adapterMonth = new ArrayAdapter<>(this, R.layout.item_simple_spinner, itemsMonth);
        adapterMonth.setDropDownViewResource(R.layout.item_simple_spinner_dropdown);
        monthSpinner.setAdapter(adapterMonth);

        List<String> itemsYear = new ArrayList<>();
        itemsYear.add("Year");
        for (int i = 2010; i >= 1950; i--)
            itemsYear.add(i + "");
        ArrayAdapter<String> adapterYear = new ArrayAdapter<>(this, R.layout.item_simple_spinner, itemsYear);
        adapterYear.setDropDownViewResource(R.layout.item_simple_spinner_dropdown);
        yearSpinner.setAdapter(adapterYear);
    }
}
