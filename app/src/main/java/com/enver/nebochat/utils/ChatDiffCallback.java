package com.enver.nebochat.utils;

import android.support.v7.util.DiffUtil;

import com.enver.nebochat.models.Chat;

import java.util.List;

/**
 * Application Controller class
 *
 * @author Enver
 * @date 18.05.2018.
 */
public class ChatDiffCallback extends DiffUtil.Callback {

    protected List<Chat> oldList, newList;

    public ChatDiffCallback(List<Chat> oldList, List<Chat> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList == null ? 0 : oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList == null ? 0 : newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
