package com.enver.nebochat.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;

import com.enver.nebochat.app.AppController;
import com.enver.nebochat.models.User;
import com.enver.nebochat.synchronization.API;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Utils class
 *
 * @author Enver
 * @date 05.05.2018.
 */
public class Utils {

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Does given month in given year has that many days
     *
     * @param day
     * @param month Starts with 0 - JANUARY
     * @param year
     * @return
     */
    public static boolean validDay(int day, int month, int year) {
        //(months begin with 0)
        Calendar mycal = new GregorianCalendar(year, month, day);

        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);

        return day <= daysInMonth;
    }

    public static class MyBooleanTypeAdapter extends TypeAdapter<Boolean> {
        @Override
        public void write(JsonWriter out, Boolean value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value(value);
            }
        }

        @Override
        public Boolean read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case BOOLEAN:
                    return in.nextBoolean();
                case NULL:
                    in.nextNull();
                    return null;
                case NUMBER:
                    return in.nextInt() != 0;
                case STRING:
                    return Boolean.parseBoolean(in.nextString());
                default:
                    throw new IllegalStateException("Expected BOOLEAN or NUMBER but was " + peek);
            }
        }
    }

    public static class MyDateTypeAdapter extends TypeAdapter<Date> {
        @Override
        public void write(JsonWriter out, Date value) throws IOException {
            if (value == null)
                out.nullValue();
            else
                out.value(value.getTime() / 1000);
        }

        @Override
        public Date read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case NULL:
                    in.nextNull();
                    return null;
                case NUMBER:
                    return new Date(in.nextLong() * 1000);
                case STRING:
                    String string = in.nextString();
                    Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(string, new ParsePosition(0));
                    if (date == null)
                        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(string, new ParsePosition(0));
                    return date;
                default:
                    throw new IllegalStateException("Expected NUMBER or STRING but was " + peek);
            }
        }
    }

    public static class MyStringArrayTypeAdapter extends TypeAdapter<RealmList<String>> {
        @Override
        public void write(JsonWriter out, RealmList<String> value) throws IOException {
            if (value == null)
                out.nullValue();
            else
                out.value(TextUtils.join(",", value));
        }

        @Override
        public RealmList<String> read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case NULL:
                    in.nextNull();
                    return null;
                case STRING:
                    String[] realmList = in.nextString().split("\\s*,\\s*");
                    return new RealmList<>(realmList);
                default:
                    throw new IllegalStateException("Expected STRING but was " + peek);
            }
        }
    }

    public static class MyUserArrayTypeAdapter extends TypeAdapter<RealmList<User>> {
        @Override
        public void write(JsonWriter out, RealmList<User> value) throws IOException {
            if (value == null)
                out.nullValue();
            else {
                List<Integer> userIds = new ArrayList<>();
                for (User user : value)
                    userIds.add(user.id);
                out.value(TextUtils.join(",", userIds));
            }
        }

        @Override
        public RealmList<User> read(JsonReader in) throws IOException {
            JsonToken peek = in.peek();
            switch (peek) {
                case NULL:
                    in.nextNull();
                    return null;
                case STRING:
                    final RealmList<User> userRealmList = new RealmList<>();
                    String[] userIds = in.nextString().split("\\s*,\\s*");
                    for (String userIdString : userIds) {
                        int userId = Integer.parseInt(userIdString);

                        Realm realm = Realm.getDefaultInstance();

                        boolean foundUser = false;
                        for (User user : realm.where(User.class).findAll())
                            if (user.id == userId) {
                                userRealmList.add(user);
                                foundUser = true;
                            }

                        realm.close();

                        if (!foundUser)
                            AppController.getInstance().api.getUser(AppController.getInstance().apiKey, this.getClass().getSimpleName(), userId).enqueue(new Callback<API.GetObjectResponse<User>>() {
                                @Override
                                public void onResponse(Call<API.GetObjectResponse<User>> call, Response<API.GetObjectResponse<User>> response) {
                                    final API.GetObjectResponse<User> getObjectResponse = response.body();

                                    if (getObjectResponse != null)
                                        switch (getObjectResponse.code) {
                                            case 600:
                                                Realm realm = Realm.getDefaultInstance();

                                                realm.executeTransaction(new Realm.Transaction() {
                                                    @Override
                                                    public void execute(Realm realm) {
                                                        realm.insertOrUpdate(getObjectResponse.object);
                                                    }
                                                });

                                                userRealmList.add(getObjectResponse.object);

                                                realm.close();
                                                break;
                                            case 601:

                                                break;
                                        }
                                }

                                @Override
                                public void onFailure(Call<API.GetObjectResponse<User>> call, Throwable t) {
                                    t.printStackTrace();
                                }
                            });
                    }
                    return userRealmList;
                default:
                    throw new IllegalStateException("Expected STRING but was " + peek);
            }
        }
    }

    private static int mAppHeight;
    private static int currentOrientation = -1;

    public static void setKeyboardVisibilityListener(final Activity activity, final KeyboardVisibilityListener keyboardVisibilityListener) {

        final View contentView = activity.findViewById(android.R.id.content);

        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private int mPreviousHeight;

            @Override
            public void onGlobalLayout() {
                int newHeight = contentView.getHeight();
                if (newHeight == mPreviousHeight)
                    return;

                mPreviousHeight = newHeight;
                if (activity.getResources().getConfiguration().orientation != currentOrientation) {
                    currentOrientation = activity.getResources().getConfiguration().orientation;
                    mAppHeight = 0;
                }

                if (newHeight >= mAppHeight)
                    mAppHeight = newHeight;

                if (newHeight != 0)
                    if (mAppHeight > newHeight)
                        // Height decreased: keyboard was shown
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
                    else
                        // Height increased: keyboard was hidden
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(false);

            }
        });
    }

    public interface KeyboardVisibilityListener {
        void onKeyboardVisibilityChanged(boolean keyboardVisible);
    }
}
