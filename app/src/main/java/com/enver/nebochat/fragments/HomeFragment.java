package com.enver.nebochat.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.enver.nebochat.R;
import com.enver.nebochat.activities.ChatActivity;
import com.enver.nebochat.models.Chat;
import com.enver.nebochat.models.Message;
import com.enver.nebochat.models.User;
import com.enver.nebochat.synchronization.Synchronization;
import com.enver.nebochat.utils.ChatDiffCallback;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * HomeFragment class
 *
 * @author Enver
 * @date 01.05.2018.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, RealmChangeListener<RealmResults<Chat>> {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    ChatsAdapter chatsAdapter;
    List<Chat> chats = new ArrayList<>();
    List<Message> lastMessages = new ArrayList<>();

    Realm realm;
    RealmResults<Chat> chatRealmResults;
    RealmResults<Message> allMessagesResults;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        chatsAdapter = new ChatsAdapter();
        recyclerView.setAdapter(chatsAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        view.findViewById(R.id.create_room_fab).setOnClickListener(this);

        realm = Realm.getDefaultInstance();

        chatRealmResults = realm.where(Chat.class).findAll();
        chatRealmResults.addChangeListener(this);
        allMessagesResults = realm.where(Message.class).findAll();
        allMessagesResults.addChangeListener(new RealmChangeListener<RealmResults<Message>>() {
            @Override
            public void onChange(@NonNull RealmResults<Message> messages) {
                HomeFragment.this.onChange(chatRealmResults);
            }
        });

        onChange(chatRealmResults);
    }

    AlertDialog newRoomDialog;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_room_fab:
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_new_room, null);
                dialogBuilder.setView(dialogView);
                dialogView.findViewById(R.id.city_view).setOnClickListener(this);
                dialogView.findViewById(R.id.city_view).setOnClickListener(this);
                newRoomDialog = dialogBuilder.show();

                break;
            case R.id.city_view:

                break;
            case R.id.country_view:

                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        realm.close();
    }

    @Override
    public void onRefresh() {
        Synchronization.performFullSync();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onChange(@NonNull RealmResults<Chat> chatRealmResults) {
        List<Chat> refreshedChats = new ArrayList<>(chatRealmResults);
        Collections.sort(refreshedChats, new Comparator<Chat>() {
            @Override
            public int compare(Chat chat, Chat t1) {
                Message lastMessage1 = realm.where(Message.class).equalTo("chatId", chat.id).sort("sent", Sort.DESCENDING).findFirst();
                Message lastMessage2 = realm.where(Message.class).equalTo("chatId", t1.id).sort("sent", Sort.DESCENDING).findFirst();

                Date date1 = lastMessage1 == null ? chat.created : lastMessage1.sent;
                Date date2 = lastMessage2 == null ? t1.created : lastMessage2.sent;

                return date1.compareTo(date2);
            }
        });
        final DiffUtil.DiffResult result = DiffUtil.calculateDiff(new ChatDiffCallback(chats, refreshedChats), true);
        chats = refreshedChats;

        lastMessages.clear();
        for (Chat chat : chats)
            lastMessages.add(realm.where(Message.class).equalTo("chatId", chat.id).sort("sent", Sort.DESCENDING).findFirst());

        result.dispatchUpdatesTo(chatsAdapter);
    }

    public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Chat chat = chats.get(position);
            Message lastMessage = lastMessages.get(position);

            holder.chatNameView.setText(StringEscapeUtils.unescapeJava(chat.name));

            if (lastMessage != null) {
                holder.lastMessageUserNameView.setVisibility(View.VISIBLE);
                holder.lastMessageView.setVisibility(View.VISIBLE);
                holder.lastUserAvatarView.setVisibility(View.VISIBLE);
                holder.seenLinearLayout.setVisibility(View.VISIBLE);
                holder.noMessagesView.setVisibility(View.GONE);

                holder.lastMessageUserNameView.setText(StringEscapeUtils.unescapeJava(lastMessage.user.nickname));
                holder.lastMessageTimeView.setText(lastMessage.getSentText());
                holder.lastMessageView.setText(StringEscapeUtils.unescapeJava(lastMessage.message));
                //TODO holder.lastUserAvatarView.setImageBitmap();
                holder.setSeenUsers(lastMessage.userSeen);
            } else {
                holder.lastMessageUserNameView.setVisibility(View.GONE);
                holder.lastMessageView.setVisibility(View.GONE);
                holder.lastUserAvatarView.setVisibility(View.GONE);
                holder.seenLinearLayout.setVisibility(View.GONE);
                holder.noMessagesView.setVisibility(View.VISIBLE);
            }
            holder.roomId = chat.id;
        }

        @Override
        public int getItemCount() {
            return chats.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            int roomId = -1;

            TextView chatNameView, lastMessageUserNameView, lastMessageTimeView, lastMessageView, noMessagesView;
            ImageView lastUserAvatarView;
            private LinearLayout seenLinearLayout;

            ViewHolder(View itemView) {
                super(itemView);

                chatNameView = itemView.findViewById(R.id.chatName);
                lastMessageUserNameView = itemView.findViewById(R.id.lastMessageUserName);
                lastMessageTimeView = itemView.findViewById(R.id.lastMessageTime);
                lastMessageView = itemView.findViewById(R.id.lastMessage);
                lastUserAvatarView = itemView.findViewById(R.id.lastUserAvatar);
                seenLinearLayout = itemView.findViewById(R.id.seenLinearLayout);
                noMessagesView = itemView.findViewById(R.id.noMessagesView);

                itemView.setOnClickListener(this);
            }

            void setSeenUsers(List<User> seenUsers) {
                seenLinearLayout.removeAllViews();

                for (User seenUser : seenUsers) {
                    View v = LayoutInflater.from(seenLinearLayout.getContext()).inflate(R.layout.item_seen_avatar, seenLinearLayout, true);
                    ImageView avatarView = v.findViewById(R.id.avatarView);
                    avatarView.setImageResource(R.drawable.ic_user);
                    //TODO avatarView.setImageBitmap();
                }
            }

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ChatActivity.class);
                intent.putExtra("chatId", roomId);
                startActivity(intent);
            }
        }
    }
}
